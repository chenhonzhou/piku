// 你的配置文件
var _config = {
  // 博客名称
  blog_name       : "Kozo4のblog",
  // github 用户名
  owner           : 'chenhonzhou',
  // github 中对应项目名
  repo            : 'issues-blog',
  // 请求量大时需要在 github 后台单独设置一个读取公开库的 token, 注意将token 拆成两个字符串，否则会被系统自动删除掉
  access_token    : '00ee4de710835843388b' + '36b669a72c2c1f718841',
  // 默认一页显示几篇文章
  per_page        : '10'
}
// 自定义
var _DIY = {
  // 你的页面图标,默认拿的是我自己的
  favicon: "http://kozo4.ooo/favicon.ico",
  // 背景,可以是颜色,也可以是背景图片 例: "rgba(130, 130, 130, 0.2)" 例: "url(1.png)"
  bg: "linear-gradient(36deg, #17522E 0%, #2893A2 60%, #FFCB16 100%)",
  // 头部背景,必须为图片
  // 下面为随机的图片
  // headBg: "https://api.isoyu.com/bing_images.php",
  headBg: "https://i.loli.net/2018/09/20/5ba39df8950eb.gif",
  // 一句话,默认拿的是一言的,你可以自己修改(如果是空就是默认的)
  slogan: "",
  // 你的头像,注意这里,文章里面的头像默认拿的是你Github上的
  avatar: "piku.svg",
  // 你的名字
  name: "Demi Kozo4",
  // 版权部分的年份,默认2018
  footer_year: "2018",
  // 版权部分的你的名称
  footer_name: "Demi Kozo4",
  // 版权部分你的链接
  footer_name_link: "http://kozo4.ooo",
  // 虽然不想写备案号但是还是要写了
  // footer_keep: "京ICP我备你他妈的案",
  // 音乐播放器[网易云],默认是歌单,如果只想是你中意的几首的话写成对象就行
  // 例: [30780812, 36921562,411755219,28051000,28381279]
  mPlayer: "2110194986",
  // mPlayer: [30780812, 36921562,411755219,28051000,28381279],
  // 关于我页面
  about: {
    // 关于你的头像,默认为你的照片里的随机一张
    avatar: "piku.svg",
    // 关于你的名字,加了个@
    name: "陈大大哦了",
    // 关于你的描述
    keyword: "伪前端，电子音乐爱好者,目标是全栈工程师🙌🙌",
    // 你的链接
    link: {
      // 注意一下都得把地址写全
      github: "http://github.com/ikozo4",
      weibo: "https://www.weibo.com/chendadaover",
      v2ex: "https://www.v2ex.com/member/chenhonzhou",
      bilibili: "https://space.bilibili.com/27013266/#/"
    }
  }
}
